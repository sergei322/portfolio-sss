module.exports = {
  purge: [
    './**/*.php',
  ],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
      lineHeight: {
        'extra-loose': '2.5',
        '12': '3rem',
       },
       zIndex: {
        '-1': '-1',
       }
    },
    backgroundColor: theme => ({
      'body': '#111',
      'theme': '#ffb400',
      'theme-secondary': '#252525',

     })
  },
  variants: {
    extend: {},
  },
  plugins: [],
}
