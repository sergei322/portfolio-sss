<?php
/**
 * Portfolio SSS functions and definitions
 *
 * @package Portfolio SSS
 * @since Portfolio SSS 1.0.0
 */

/********************
****** ACTIONS ******
********************/


/**
 * Add them supports
 */
if( !function_exists( 'prtflsss_theme_support' ) ) {
    function prtflsss_theme_support() {
    
         // Add default posts and comments RSS feed links to head.
        add_theme_support( 'automatic-feed-links' );
    
        // Enable support for Post Thumbnails on posts and pages.
        add_theme_support( 'post-thumbnails' );
    
        // TODO: Set post thumbnail size.
        // set_post_thumbnail_size( 1200, 9999 );
    
        // TODO: Add custom image size used in Cover Template.
        // add_image_size( 'prtflsss-fullscreen', 1980, 9999 );
    
        // Custom logo.
        add_theme_support( 'custom-logo' );
    
        // <title> tag in the document head
        add_theme_support( 'title-tag' );
    
        // output valid HTML5
        add_theme_support(
            'html5',
            array(
                'search-form',
                'comment-form',
                'comment-list',
                'gallery',
                'caption',
                'script',
                'style',
                'navigation-widgets',
            )
        );
    
        // textdomain
        load_theme_textdomain( 'prtflsss', get_template_directory().'/languages' );
    
        // Add support for full and wide align images.
        add_theme_support( 'align-wide' );
    
        // Add support for responsive embeds.
        add_theme_support( 'responsive-embeds' );
    }
}


/**
 * Enqueue styles and scripts.
 */
if( !function_exists( 'prtflsss_enqueue_scripts' ) ) {
    function prtflsss_enqueue_scripts() {
        wp_enqueue_style( 'prtflsss-main', get_stylesheet_directory_uri() . '/css/style.css', array(), filemtime(get_template_directory() . '/style.css'), false) ;
    }
}

/**
 * Register navigation menus.
 */
if( !function_exists( 'prtflsss_menus' ) ) {
    function prtflsss_menus() {
        
        $locations = array(
            'header'  => __( 'Main menu', 'prtflsss' ),
        );
    
        register_nav_menus( $locations );
    }
}

/**
 * Add menu icons
 */
if( !function_exists( 'prtflsss_nav_menu_item_custom_fields' ) ) {
    function prtflsss_nav_menu_item_custom_fields( $item_id, $item ) {
        wp_nonce_field( 'prtflsss_menu_custom_fields', '_prtflsss_menu_custom_fields' );
        $icon = get_post_meta( $item_id, 'prtflsss_menu_icon', true );
        $icon_packs = prtflsss_get_icons(); ?>
        <div class="description-wide" style="margin: 5px 0;">
            <span class="description">
                <?php _e( 'Menu icon', 'prtflsss' ); ?>
            </span>
            <br />
            <input type="hidden" class="nav-menu-id" value="<?php echo $item_id ;?>" />
            <select name="prtflsss-menu-icon[<?php echo $item_id ;?>]" id="prtflsss-menu-icon-<?php echo $item_id ;?>">
                <option value=""><?php _e( 'Select an icon', 'ssspprtflsss' ) ?></option>

                <?php if( !empty( $icon_packs ) ) :
                    foreach( $icon_packs as $icon_pack_name => $icons ) : ?>
                        <optgroup label="<?php echo $icon_pack_name; ?>">
                            <?php foreach( $icons as $name => $path ) : ?>
                                <option value="<?php echo $path; ?>" <?php selected( $path, $icon ); ?>>
                                    <?php echo $name; ?>
                                </option>
                            <?php endforeach; ?>
                        </optgroup>
                    <?php endforeach; 
                endif; ?>
            </select>
        </div>
    <?php }
}

/**
* Save the menu item meta
* 
*/
if( !function_exists( 'prtflsss_update_nav_menu_item' ) ) {
    function prtflsss_update_nav_menu_item( $menu_id, $menu_item_db_id ) {
        if ( ! isset( $_POST['_prtflsss_menu_custom_fields'] ) || ! wp_verify_nonce( $_POST['_prtflsss_menu_custom_fields'], 'prtflsss_menu_custom_fields' ) ) {
            return $menu_id;
        }
        if ( isset( $_POST['prtflsss-menu-icon'][$menu_item_db_id]  ) ) {
            $sanitized_data = sanitize_text_field( $_POST['prtflsss-menu-icon'][$menu_item_db_id] );
            update_post_meta( $menu_item_db_id, 'prtflsss_menu_icon', $sanitized_data );
        } else {
            delete_post_meta( $menu_item_db_id, 'prtflsss_menu_icon' );
        }
    }
}


add_action( 'after_setup_theme',                'prtflsss_theme_support' );
add_action( 'init',                             'prtflsss_menus' );
add_action( 'wp_enqueue_scripts',               'prtflsss_enqueue_scripts' );
add_action( 'wp_nav_menu_item_custom_fields',   'prtflsss_nav_menu_item_custom_fields', 10, 2 );
add_action( 'wp_update_nav_menu_item',          'prtflsss_update_nav_menu_item', 10, 2 );


/********************
****** FILTERS ******
********************/

// Add async and defer to scripts tag
if( !function_exists( 'prtflsss_script_loader_tag' ) ) {
    function prtflsss_script_loader_tag( $tag, $handle ) {
        foreach ( array( 'async', 'defer' ) as $attr ) {
            if ( ! wp_scripts()->get_data( $handle, $attr ) ) {
                continue;
            }
            // Prevent adding attribute when already added in #12009.
            if ( ! preg_match( ":\s$attr(=|>|\s):", $tag ) ) {
                $tag = preg_replace( ':(?=></script>):', " $attr", $tag, 1 );
            }
            // Only allow async or defer, not both.
            break;
        }
        return $tag;
    }
}

// Add classes for body
if( !function_exists( 'prtflsss_body_class' ) ) {
    function prtflsss_body_class( $classes ) {
        return array_merge( $classes, array( 'bg-body' ) );
    }
}

// Change menu link title attributes
if( !function_exists( 'prtflsss_nav_menu_link_attributes' ) ) {
    function prtflsss_nav_menu_link_attributes( $atts, $item, $args, $depth ) {
        $classes = array(
            'block',
            'p-0',
            'w-12',
            'h-12',
        );
        if( !empty( $atts['class'] ) ) {
            $classes[] = $atts['class'];
        }
        $atts['class'] = implode( ' ', $classes );
        return $atts;
    }
}

// Change menu items classes
if( !function_exists( 'prtflsss_nav_menu_css_class' ) ) {
    function prtflsss_nav_menu_css_class( $classes, $item, $args, $depth ) {
        $classes = array(
            'items-center',
            'relative',
            'transition-all',
            'duration-300',
            'my-5',
            'text-white',
            'uppercase',
            'w-12',
            'h-12',
            'rounded-full'
        );
        if( $item->current ) {
            $classes[] = 'bg-theme';
        } else {
            $classes[] = 'bg-theme-secondary';
            $classes[] = 'hover:bg-theme';
        }
        return $classes;
    }
}

// Change menu title markup
if( !function_exists( 'prtflsss_nav_menu_item_title' ) ) {
    function prtflsss_nav_menu_item_title( $title, $item, $args, $depth ) {
        $classes = array(
            'bg-theme',
            'leading-12', 
            'text-base',
            '-z-1',
            'transition-all',
            'duration-300',
            'absolute',
            'opacity-0',
            'top-0',
            'right-0',
            'rounded-full',
            'pr-8',
            'pl-7',
            'h-12',
            'overflow-hidden'
        );
        $icon_path = get_post_meta( $item->db_id, 'prtflsss_menu_icon', true );
        $icon = '';
        if( !empty( $icon_path ) ) {
            $icon = prtflsss_get_svg( $icon_path, array( 'w-10 pl-2 pt-2' ) );
        } else {
            $first_letter = substr( $title, 0, 1 );
            $first_letter = substr( $title, 0, 1 );
            $icon = '<span class="font-bold text-center text-3xl leading-12 block">' . $first_letter . '</span>';
        }

        $classes = implode( ' ', $classes );
        return $icon . '<span class="' . $classes . '">' . $title . '</span>';
    }
}

add_filter( 'script_loader_tag',            'prtflsss_script_loader_tag', 10, 2 );
add_filter( 'body_class',                   'prtflsss_body_class' );
add_filter( 'nav_menu_css_class',           'prtflsss_nav_menu_css_class', 10, 4 );
add_filter( 'nav_menu_link_attributes',     'prtflsss_nav_menu_link_attributes', 10, 4 );
add_filter( 'nav_menu_item_title',          'prtflsss_nav_menu_item_title', 10, 4 );



/****************************
****** THEME FUNCTIONS ******
****************************/

// Get icons list
if( !function_exists( 'prtflsss_get_icons' ) ) {
    function prtflsss_get_icons( $icons_dir = '' ) {
        $icons_dir = !empty( $icons_dir ) ?: get_stylesheet_directory() . DIRECTORY_SEPARATOR . 'icons' . DIRECTORY_SEPARATOR;
        if( !empty( $icons_list_raw = $icons_list_raw = list_files( $icons_dir ) ) ) {
            $icons_list_raw = str_replace( $icons_dir, '', $icons_list_raw );
            $icons_list = array();
            foreach( $icons_list_raw as $icons_list_raw_item ) {
                $exploded = explode( DIRECTORY_SEPARATOR, $icons_list_raw_item );
                $name = array_pop( $exploded );
                $icons_pack = implode( '_', $exploded );
                $icons_list[$icons_pack][$name] = $icons_list_raw_item;
                
            }
            return $icons_list;
        }
        return false;

    }
}

// Get svg icon
if( !function_exists( 'prtflsss_get_svg' ) ) {
    function prtflsss_get_svg( $path, $classes = array(), $icons_dir = '' ) {
        $icons_dir = !empty( $icons_dir ) ?: get_stylesheet_directory() . DIRECTORY_SEPARATOR . 'icons' . DIRECTORY_SEPARATOR;
        $classes = implode( ' ', $classes );
        $svg = @file_get_contents( $icons_dir . $path );
        return str_replace( '<svg', '<svg class="' . $classes . '"', $svg );
    }
}