<?php 
/**
 * The header for theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @package Portfolio SSS
 * @since Portfolio SSS 1.0.0
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="profile" href="https://gmpg.org/xfn/11" />
    <?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
<header id="header" class="fixed right-8 flex items-center top-1/2 transform -translate-y-2/4">
    <?php wp_nav_menu(
        array(
            'depth' => 1,
        )
    ); ?>
</header>